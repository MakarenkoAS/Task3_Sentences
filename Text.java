package ua.org.oa.makarenkoas;

import java.io.*;

/*
 * @version 1.0 06 Mar 2017 
 * @author Makarenko Alexander
 */

public class Text {

	@SuppressWarnings("resource")
	public static StringBuilder readFile() throws IOException {
		// Method to read text from file
		
		StringBuilder text = new StringBuilder();
		BufferedReader in = new BufferedReader(new FileReader("fileFolder\\text.txt"));
		String s;
		while ((s = in.readLine()) != null) {
			text.append(s);
			text.append("\n");
		}

		return text;
	}

	public static void sortByWords() throws IOException {
		// Method for sorting sentences by words
		
		String sentences[] = readFile().toString().trim().split("[.!?]\\s*"); // Split text to sentences
		
		int words[] = new int[sentences.length];							  // Two equal arrays
		int words2[] = new int[sentences.length];							  // to compare quantity of words

		for (int i = 0; i <= sentences.length - 1; i++) {	// Loop to calculate words in sentence
			int countWords = 0;
			String splitByWords[] = sentences[i].toString().trim().split(" ");
			for (int j = 0; j <= splitByWords.length - 1; j++) {
				countWords++;
			}
			words[i] = countWords;
			words2[i] = countWords;
		}
		
		for (int i = 0; i <= words.length - 1; i++) {		// Loop to check sentences with equal number of words
			for (int j = i + 1; j <= words.length - 1; j++) {
				if (words[i] == words[j]) {
					System.out.println("Sentences: " + sentences[i] + " ~~ & ~~ " + "\n" + sentences[j]
							+ " ~~ - have equal quantity of words.");
					System.out.println();
				}
			}
		}
		
		// Loops and if statements to sort sentences by words
		int minWordsQuantity = 0;
		int wordsInSentence = 0;
		int index = 0;
		for (int i = 0; i <= words.length - 1; i++) {
			minWordsQuantity = words[i];
			index = i;
			for (int j = i + 1; j <= words.length - 1; j++) {
				if (minWordsQuantity > words[j]) {
					minWordsQuantity = words[j];
					index = j;
				}
			}
			if (index != i) {
				wordsInSentence = words[i];
				words[i] = words[index];
				words[index] = wordsInSentence;
			}
		}
		int arrayIndex = 0;
		for (int i = 0; i <= words.length - 1; i++) {
			for (int j = 0; j <= words.length - 1; j++) {
				if (words[i] == words2[j]) {
					arrayIndex =j;
				}
			}
			System.out.println("Quantity of words in sentence: ~~ " + sentences[arrayIndex] + " ~~ is: " + words[i] + " words.");
		}
	}
}