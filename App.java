package ua.org.oa.makarenkoas;

import java.io.IOException;

/*
 * @version 1.0 06 Mar 2017 
 * @author Makarenko Alexander
 */

public class App {

	public static void main(String[] args) throws IOException {	
	    System.out.println(Text.readFile());
	    Text.sortByWords();
	    
	    //Text.sortByWords(Text.searchSymbols());
	}

}
